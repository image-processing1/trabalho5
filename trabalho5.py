import os
import sys
from skimage import io
import numpy as np
import cv2
import argparse


def surf_match(img1, img2):
	surf = cv2.xfeatures2d.SURF_create(400)
	# find the keypoints and descriptors
	kp1, desc1 = surf.detectAndCompute(img1, None)
	kp2, desc2 = surf.detectAndCompute(img2, None)

	# Match descriptors.
	bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)
	matches = bf.match(desc1, desc2)

	# Sort them in the order of their distance.
	matches = sorted(matches, key = lambda x:x.distance)
	return kp1, kp2, matches


def sift_match(img1, img2):
	sift = cv2.xfeatures2d.SIFT_create()

	# find the keypoints and descriptors
	kp1, desc1 = sift.detectAndCompute(img1, None)
	kp2, desc2 = sift.detectAndCompute(img2, None)

	# Match descriptors.
	bf = cv2.BFMatcher(cv2.NORM_L2, crossCheck=True)
	matches = bf.match(desc1, desc2)

	# Sort them in the order of their distance.
	matches = sorted(matches, key = lambda x:x.distance)
	return kp1, kp2, matches


def brief_match(img1, img2):
	# Initiate FAST detector
	star = cv2.xfeatures2d.StarDetector_create()
	# Initiate BRIEF extractor
	brief = cv2.xfeatures2d.BriefDescriptorExtractor_create()

	# find the keypoints and descriptors
	kp1 = star.detect(img1, None)
	kp1, desc1 = brief.compute(img1, kp1)
	kp2 = star.detect(img2, None)
	kp2, desc2 = brief.compute(img2, kp2)

	# Match descriptors.
	bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
	matches = bf.match(desc1, desc2)

	# Sort them in the order of their distance.
	matches = sorted(matches, key = lambda x:x.distance)
	return kp1, kp2, matches


def orb_match(img1, img2):
	orb = cv2.ORB_create()
	# find the keypoints and descriptors
	kp1, desc1 = orb.detectAndCompute(img1, None)
	kp2, desc2 = orb.detectAndCompute(img2, None)

	# Match descriptors.
	bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
	matches = bf.match(desc1, desc2)

	# Sort them in the order of their distance.
	matches = sorted(matches, key = lambda x:x.distance)
	return kp1, kp2, matches


def select(matches, th):
	n = int(len(matches)*th)
	return matches[:n]


def to_rgb(bgr):
	return cv2.cvtColor(bgr, cv2.COLOR_BGR2RGB)


def draw_kpts(kpts, img, show=False):
	img = cv2.drawKeypoints(img, kp1, None, flags=2)
	if(show):
		io.imshow(img)
		io.show()
	return img


def draw_matches(img1, img2, kp1, kp2, matches, show=False):
	img = cv2.drawMatches(to_rgb(img1), kp1, to_rgb(img2), kp2, matches, None, flags=2)
	if(show):
		io.imshow(img)
		io.show()
	return img


def show_bgr(img):
	img = to_rgb(img)
	io.imshow(img)
	io.show()


if __name__ == '__main__':

	parser = argparse.ArgumentParser()
	parser.add_argument("img1", help="The path to the first image")
	parser.add_argument("img2", help="The path to the second image")
	parser.add_argument("--descriptor",
						help="The descriptor to use",
						default='sift')
	parser.add_argument("--threshold", type=float,
		help="This threshold is between 0 and 1 and defines the range of values\
		accepted as a good match", default=0.3)
	args = parser.parse_args()

	path1 = args.img1
	path2 = args.img2

	th = args.threshold
	img1 = cv2.imread(path1)
	img2 = cv2.imread(path2)
	print("Shape img 1: ", img1.shape, " Type: ", img1.dtype)
	print("Shape img 2: ", img2.shape, " Type: ", img2.dtype)
	os.makedirs('results/', exist_ok=True)

	#Step 1
	img1_gr = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
	img2_gr = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

	#Step 2 and 3
	if(args.descriptor == 'sift'):
		kp1, kp2, matches = sift_match(img1_gr, img2_gr)
	elif(args.descriptor == 'orb'):
		kp1, kp2, matches = orb_match(img1_gr, img2_gr)
	elif(args.descriptor == 'surf'):
		kp1, kp2, matches = surf_match(img1_gr, img2_gr)
	elif(args.descriptor == 'brief'):
		kp1, kp2, matches = brief_match(img1_gr, img2_gr)

	img_kpts = draw_kpts(kp1, img1_gr)
	io.imsave('results/'+args.descriptor+'_kpts'+path1.split('/')[-1].split('.')[0]
			  +'_'+str(args.threshold)+'.jpg', img_kpts)
	#Step 4
	matches = select(matches, th)
	img3 = draw_matches(img1, img2, kp1, kp2, matches)
	io.imsave('results/'+args.descriptor+'_matches'+path1.split('/')[-1].split('.')[0]
			  +'_'+path2.split('/')[-1].split('.')[0]+'_'+str(args.threshold)+'.jpg', img3)

	src_pts = np.float32([kp1[m.queryIdx].pt for m in matches]).reshape(-1,1,2)
	dst_pts = np.float32([kp2[m.trainIdx].pt for m in matches]).reshape(-1,1,2)

	#Step 5
	M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
	print('Homography ', M)

	h1, w1, _ = img1.shape
	h2, w2, _ = img2.shape
	#Step 6
	warp1 = cv2.warpPerspective(img1, M, (w1+w2, h2))

	#Step 7
	warp1[0:h2, 0:w2] = img2

	#show_bgr(warp1)
	io.imsave('results/'+args.descriptor+'_panoramic'+path1.split('/')[-1].split('.')[0]
			  +'_'+path2.split('/')[-1].split('.')[0]+'_'+str(args.threshold)+'.jpg', to_rgb(warp1))
